/**
 * Created by Sko on 18/11/2016.
 */

var GLOBAL_THEMES = {
    colors : {
        grey : {
            0 : '#424242',
            1 : '#616161',
            2 : '#757575',
            3 : '#9E9E9E',
            4 : '#BDBDBD',
            5 : '#E0E0E0',
            6 : '#EEEEEE',
            7 : '#F5F5F5'
        },
        blue : {
            0 : '#1565C0',
            1 : '#1976D2',
            2 : '#1E88E5',
            3 : '#2196F3',
            4 : '#42A5F5',
            5 : '#64B5F6',
            6 : '#90CAF9',
            7 : '#BBDEFB'
        },
        purple : {
            0 : '#6A1B9A',
            1 : '#7B1FA2',
            2 : '#8E24AA',
            3 : '#9C27B0',
            4 : '#AB47BC',
            5 : '#BA68C8',
            6 : '#CE93D8',
            7 : '#E1BEE7'
        },
        green : {
            0 : '#2E7D32',
            1 : '#388E3C',
            2 : '#43A047',
            3 : '#4CAF50',
            4 : '#66BB6A',
            5 : '#81C784',
            6 : '#A5D6A7',
            7 : '#C8E6C9'
        },
        yellow : {
            0 : '#F9A825',
            1 : '#FBC02D',
            2 : '#FDD835',
            3 : '#FFEB3B',
            4 : '#FFEE58',
            5 : '#FFF176',
            6 : '#FFF59D',
            7 : '#FFF9C4'
        },
        orange : {
            0 : '#EF6C00',
            1 : '#F57C00',
            2 : '#FB8C00',
            3 : '#FF9800',
            4 : '#FFA726',
            5 : '#FFB74D',
            6 : '#FFCC80',
            7 : '#FFE0B2'
        },
        lightGreen : {
            0 : '#558B2F',
            1 : '#689F38',
            2 : '#7CB342',
            3 : '#8BC34A',
            4 : '#9CCC65',
            5 : '#AED581',
            6 : '#C5E1A5',
            7 : '#DCEDC8'
        },
        lightBlue : {
            0 : '#0277BD',
            1 : '#0288D1',
            2 : '#039BE5',
            3 : '#03A9F4',
            4 : '#29B6F6',
            5 : '#4FC3F7',
            6 : '#81D4FA',
            7 : '#B3E5FC'
        },
        pink : {
            0 : '#AD1457',
            1 : '#C2185B',
            2 : '#D81B60',
            3 : '#E91E63',
            4 : '#EC407A',
            5 : '#F06292',
            6 : '#F48FB1',
            7 : '#F8BBD0'
        },
        lime : {
            0 : '#9E9D24',
            1 : '#AFB42B',
            2 : '#C0CA33',
            3 : '#CDDC39',
            4 : '#D4E157',
            5 : '#DCE775',
            6 : '#E6EE9C',
            7 : '#F0F4C3'
        }
    },
    index : [
        'blue',
        'pink',
        'lightBlue',
        'purple',
        'yellow',
        'lime',
        'green',
        'orange',
        'lightGreen',
        'grey'
    ]
};

var GLOBAL_CONTROLLER = {
    behavior : {
        current : 'mirror',
        last : null
    },
    line : {
        thickness : 4,
        fragmentNbr : 500,
        StressRange : 60,
        smooth: 0.8,
        fftSize : 2048
    },
    theme : {
        fill : {
            current: 'blue',
            last : null
        },
        stroke: {
            current: 'lightBlue',
            last : null
        }
    }
};

window.requestAnimationFrame = (function() {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function(callback) {
            windows.setTimeout(callback, 1000 / 60);
        };
})();

window.AudioContext = window.AudioContext || window.webkitAudioContext;

var ControlPanel = (function () {

    var controlPanel = {

        init : function() {

            this.el = document.querySelector('.spectrumSettingsPanel-container');

            this.songName = {};
            this.songName.el = document.querySelector('#songName');

            this.getDOMElements();

            this.bindEvents();
        },

        getDOMElements : function() {

            this.behavior = {};
            this.behavior.linear = document.querySelector('#linear');
            this.behavior.anarchist = document.querySelector('#anarchist');
            this.behavior.mirror = document.querySelector('#mirror');

            this.line = {};
            this.line.thickness = document.querySelector('#lineThickness');
            this.line.thicknessLabel = document.querySelector('#lineThicknessLabel');
            this.line.fragmentNbr = document.querySelector('#lineFragmentsNbr');
            this.line.FragmentsNbrLabel = document.querySelector('#lineFragmentsNbrLabel');
            this.line.StressRange = document.querySelector('#lineStressRange');
            this.line.StressRangeLabel = document.querySelector('#lineStressRangeLabel');

            this.theme = {};
            this.theme.fill = [];
            this.theme.stroke = [];

            this.theme.fill.length = 0;
            this.theme.stroke.length = 0;

            // Getting all the color buttons
            for(var i = 0; i < GLOBAL_THEMES.index.length; i++) {
                var colorUC = GLOBAL_THEMES.index[i].capitalizeFirstLetter();
                this.theme.fill[GLOBAL_THEMES.index[i] + 'Btn'] = document.querySelector('#fill' + colorUC);
                this.theme.stroke[GLOBAL_THEMES.index[i] + 'Btn'] = document.querySelector('#stroke' + colorUC);

                this.theme.fill.length += 1;
                this.theme.stroke.length += 1;
            }

            this.h3 = this.el.querySelectorAll('h3.title');
            this.rangeContainer = this.el.querySelectorAll('.range-container');
        },

        bindEvents : function() {
            this.behavior.linear.addEventListener('click', this.setBehavior.bind(this));
            this.behavior.anarchist.addEventListener('click', this.setBehavior.bind(this));
            this.behavior.mirror.addEventListener('click', this.setBehavior.bind(this));

            this.line.thickness.addEventListener('input', this.setLineThickness.bind(this));
            this.line.fragmentNbr.addEventListener('input', this.setFragmentNbr.bind(this));
            this.line.StressRange.addEventListener('input', this.setLineStressRange.bind(this));

            // Setting events on all the color buttons
            for(var i = 0; i < GLOBAL_THEMES.index.length; i++) {
                this.theme.fill[GLOBAL_THEMES.index[i] + 'Btn'].addEventListener('click', this.setFillColor.bind(this));
                this.theme.stroke[GLOBAL_THEMES.index[i] + 'Btn'].addEventListener('click', this.setStrokeColor.bind(this));
            }

            // bind dynamically value of fragmentNbr range, NEED TO BE COMPLETED for other ranges
            this.line.fragmentNbr.value = GLOBAL_CONTROLLER.line.fragmentNbr;
            this.line.FragmentsNbrLabel.textContent = GLOBAL_CONTROLLER.line.fragmentNbr;

            this.line.thickness.value = GLOBAL_CONTROLLER.line.thickness;
            this.line.thicknessLabel.textContent = GLOBAL_CONTROLLER.line.thickness;

            this.line.StressRange.value = GLOBAL_CONTROLLER.line.StressRange;
            this.line.StressRangeLabel.textContent = GLOBAL_CONTROLLER.line.StressRange;
        },

        setBehavior : function(e) {

            var value = e.currentTarget.getAttribute('sk-value');

            GLOBAL_CONTROLLER.behavior.last = GLOBAL_CONTROLLER.behavior.current;
            GLOBAL_CONTROLLER.behavior.current = value;

            this.behavior[GLOBAL_CONTROLLER.behavior.last].classList.toggle('btn-active');
            this.behavior[GLOBAL_CONTROLLER.behavior.current].classList.toggle('btn-active');
        },

        setFragmentNbr : function(e) {
            GLOBAL_CONTROLLER.line.fragmentNbr = parseInt(e.currentTarget.value);
            this.line.FragmentsNbrLabel.textContent = parseInt(e.currentTarget.value);
        },

        setLineThickness : function(e) {

            GLOBAL_CONTROLLER.line.thickness = parseInt(e.currentTarget.value);
            this.line.thicknessLabel.textContent = parseInt(e.currentTarget.value);
        },

        setLineStressRange : function(e) {

            GLOBAL_CONTROLLER.line.StressRange = parseInt(e.currentTarget.value);
            this.line.StressRangeLabel.textContent = parseInt(e.currentTarget.value);
        },

        setFillColor : function(e) {

            var value = e.currentTarget.getAttribute('sk-value');

            GLOBAL_CONTROLLER.theme.fill.last = GLOBAL_CONTROLLER.theme.fill.current;
            GLOBAL_CONTROLLER.theme.fill.current = value;

            this.theme.fill[GLOBAL_CONTROLLER.theme.fill.last + 'Btn'].classList.toggle('btn-active');
            this.theme.fill[GLOBAL_CONTROLLER.theme.fill.current + 'Btn'].classList.toggle('btn-active');

            this.songName.el.style.color = GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.fill.current][3];
        },

        setStrokeColor : function(e) {

            var value = e.currentTarget.getAttribute('sk-value');

            GLOBAL_CONTROLLER.theme.stroke.last = GLOBAL_CONTROLLER.theme.stroke.current;
            GLOBAL_CONTROLLER.theme.stroke.current = value;

            this.theme.stroke[GLOBAL_CONTROLLER.theme.stroke.last + 'Btn'].classList.toggle('btn-active');
            this.theme.stroke[GLOBAL_CONTROLLER.theme.stroke.current + 'Btn'].classList.toggle('btn-active');
        },

        firstShow : function() {

            // display color buttons
            var i;
            var btnDelay = 0;
            for(i = 0; i < this.theme.fill.length; i++) {
                TweenMax.to(this.theme.stroke[GLOBAL_THEMES.index[i] + 'Btn'], 2, {opacity: 1, delay: btnDelay});
                TweenMax.to(this.theme.fill[GLOBAL_THEMES.index[i] + 'Btn'], 2, {opacity: 1, delay: btnDelay});
                btnDelay += 0.1;
            }

            // display behavior buttons
            TweenMax.to(this.behavior.linear, 0, {x: -20});
            TweenMax.to(this.behavior.mirror, 0, {x: 20});

            TweenMax.to(this.behavior.linear, 1, {opacity: 1, x: 0});
            TweenMax.to(this.behavior.anarchist, 1, {opacity: 1});
            TweenMax.to(this.behavior.mirror, 1, {opacity: 1, x: 0});

            // display titles
            for(i = 0; i < this.h3.length; i++) {
                TweenMax.to(this.h3[i], 1, {opacity: 1, delay: 1.5});
            }

            // display ranges
            var rangeDelay = 0;
            for(i = 0; i < this.rangeContainer.length; i++) {
                TweenMax.to(this.rangeContainer[i], 2, {opacity: 1, delay: rangeDelay});
                rangeDelay += 0.3;
            }

            //TOTAL ANIM TIME = 2.9s
        },

        show : function() {
            var i, showDuration = 1;

            // show color buttons
            for(i = 0; i < this.theme.fill.length; i++) {
                TweenMax.to(this.theme.stroke[GLOBAL_THEMES.index[i] + 'Btn'], showDuration, {opacity: 1});
                TweenMax.to(this.theme.fill[GLOBAL_THEMES.index[i] + 'Btn'], showDuration, {opacity: 1});
            }

            // show behavior buttons
            TweenMax.to(this.behavior.linear, showDuration, {opacity: 1});
            TweenMax.to(this.behavior.anarchist, showDuration, {opacity: 1});
            TweenMax.to(this.behavior.mirror, showDuration, {opacity: 1});

            // show titles
            for(i = 0; i < this.h3.length; i++) {
                TweenMax.to(this.h3[i], showDuration, {opacity: 1});
            }

            // show ranges
            for(i = 0; i < this.rangeContainer.length; i++) {
                TweenMax.to(this.rangeContainer[i], showDuration, {opacity: 1});
            }

            //TOTAL ANIM TIME = 1s
        },

        hide : function() {
            var i, fadeDuration = 1;

            // hide color buttons
            for(i = 0; i < this.theme.fill.length; i++) {
                TweenMax.to(this.theme.stroke[GLOBAL_THEMES.index[i] + 'Btn'], fadeDuration, {opacity: 0});
                TweenMax.to(this.theme.fill[GLOBAL_THEMES.index[i] + 'Btn'], fadeDuration, {opacity: 0});
            }

            // hide behavior buttons
            TweenMax.to(this.behavior.linear, fadeDuration, {opacity: 0});
            TweenMax.to(this.behavior.anarchist, fadeDuration, {opacity: 0});
            TweenMax.to(this.behavior.mirror, fadeDuration, {opacity: 0});

            // hide titles
            for(i = 0; i < this.h3.length; i++) {
                TweenMax.to(this.h3[i], fadeDuration, {opacity: 0});
            }

            // hide ranges
            for(i = 0; i < this.rangeContainer.length; i++) {
                TweenMax.to(this.rangeContainer[i], fadeDuration, {opacity: 0});
            }

            //TOTAL ANIM TIME = 1s
        }
    };

    return controlPanel;

})();

var spectrum = (function () {

    var Spectrum = {

        canvas : {},
        drawValues : {},
        spectrums : null,

        init : function() {

            this.getCanvas();
        },

        getCanvas : function() {

            this.canvas.el = document.querySelector('.spectrum');
            this.canvas.context = this.canvas.el.getContext('2d');

            this.canvas.height = this.canvas.el.height;
            this.canvas.width = this.canvas.el.width;

            this.canvas.centerH = this.canvas.height / 2;
            this.canvas.centerW = this.canvas.width / 2;
        },

        buildUnitSpectrums : function(nbrUnitSpectrum, spectrumsCarac) {

            this.spectrums = [];

            var i;
            for(i = 0; i < nbrUnitSpectrum; i++) {
                this.spectrums[i] = new UnitSpectrum();
                this.spectrums[i].init(spectrumsCarac[i].r, this.canvas);
            }
        },

        firstShow : function() {

            var resetData = [];

            var i;
            for(i = 0; i < GLOBAL_CONTROLLER.line.fragmentNbr; i++) {
                resetData[i] = 0;
            }

            //clear Canvas
            this.canvas.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

            TweenMax.to(this.canvas.el, 0, {scale : 0});
            TweenMax.to(this.canvas.el, 0.5, {opacity: 1, scale : 1, delay : 2});

            // Rendering every spectrum
            for(i = 0; i < this.spectrums.length; i++) {
                this.spectrums[i].render(
                    resetData,
                    GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.fill.current][i],
                    GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.stroke.current][i]
                );
            }

            //TOTAL ANIM TIME = 0.5s
        },

        show : function() {

            TweenMax.to(this.canvas.el, 1, {opacity: 1});

            //TOTAL ANIM TIME = 1s
        },

        hide : function() {

            var resetData = [];

            var i;
            for(i = 0; i < GLOBAL_CONTROLLER.line.fragmentNbr; i++) {
                resetData[i] = 0;
            }

            var tl = new TimelineMax();

            tl.to(this.canvas.el, 1, {opacity: 0})
                .add((function() {
                    //clear Canvas
                    this.canvas.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

                    // Rendering every spectrum
                    for(i = 0; i < this.spectrums.length; i++) {
                        this.spectrums[i].render(
                            resetData,
                            GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.fill.current][i],
                            GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.stroke.current][i]
                        );
                    }
                }).bind(this));

            //TOTAL ANIM TIME = 1s
        }
    };

    var UnitSpectrum = function(){
        this.context = null;
        this.r = null;
        this.center = null;
    };

    UnitSpectrum.prototype.init = function(r, canvas) {
        this.r = r;
        this.context = canvas.context;
        this.center = {
            x : canvas.width / 2,
            y : canvas.height / 2
        };
    };

    UnitSpectrum.prototype.render = function(data, fillColor, strokeColor) {

        this.context.beginPath();

        var stressRange = GLOBAL_CONTROLLER.line.StressRange;

        var i, deg, rad, increasedLength;
        for(i = 0; i < data.length; i++) {

            increasedLength = data[i] * stressRange / 255;

            deg = (360/data.length) * i;
            rad = deg * Math.PI / 180;

            var x = (this.r + increasedLength) * Math.cos(rad) + (this.center.x);
            var y = (this.r + increasedLength) * Math.sin(rad) + (this.center.y);

            if(i == 0) {
                this.context.moveTo(x, y);
            } else {
                this.context.lineTo(x, y);
            }
        }

        increasedLength = data[0] * stressRange / 255;

        deg = 0;
        rad = deg * Math.PI / 180;

        x = (this.r + increasedLength) * Math.cos(rad) + (this.center.x);
        y = (this.r + increasedLength) * Math.sin(rad) + (this.center.y);

        this.context.lineTo(x, y);

        this.context.lineWidth = GLOBAL_CONTROLLER.line.thickness;
        this.context.lineJoin = 'round';
        this.context.lineCap = 'round';
        this.context.strokeStyle = strokeColor;
        this.context.fillStyle = fillColor;
        this.context.stroke();
        this.context.fill();
        this.context.closePath();
    };

    return Spectrum;

})();

var app = (function (sp, pc) {

    var App = {

        spectrum : null,
        audioVisualizer : null,
        controlPanel : null,
        nbrUnitSpectrum : null,
        UnitSpectrumData : null,
        nbrDataBySpectrum : null,
        launchEls : {},
        animationFrame : undefined,
        songName : {},
        searchLocalFile : null,

        init : function() {

            this.el = document.querySelector('html');

            this.spectrum = sp;
            this.controlPanel = pc;

            this.build();
            this.bindEvents();

            this.launch();
        },

        build : function() {

            this.controlPanel.init();

            this.spectrumsCarac = [
                { r : 320 },
                { r : 280 },
                { r : 240 },
                { r : 200 },
                { r : 160 },
                { r : 120 },
                { r : 80 },
                { r : 40 }
            ];

            this.nbrUnitSpectrum = this.spectrumsCarac.length;

            this.spectrum.init();
            this.spectrum.buildUnitSpectrums(this.nbrUnitSpectrum, this.spectrumsCarac);

            this.songName.el = this.el.querySelector('#songName');
            this.songName.el.style.color = GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.fill.current][3];

            this.searchLocalFile = document.querySelector('#searchLocalFile');
            this.searchLocalFile.addEventListener("change", (function (e) {

                e.stopPropagation();
                e.preventDefault();

                var file = e.target.files[0];

                this.songDropped(file);

                console.log('searched');
            }).bind(this));
        },

        bindEvents : function() {
            this.el.addEventListener('newSoundLoaded', this.render.bind(this));
        },

        handleDrop : function() {

        document.body.addEventListener("dragenter", function () {}, false);

        document.body.addEventListener("dragover", function (e) {
            e.stopPropagation();
            e.preventDefault();
            e.dataTransfer.dropEffect = 'copy';
        });

        document.body.addEventListener("dragleave", function () {});

        document.body.addEventListener("drop", (function (e) {

            e.stopPropagation();
            e.preventDefault();

            var file = e.dataTransfer.files[0];

            this.songDropped(file);

            console.log('dropped');
        }).bind(this));
    },

        render : function() {

            this.nbrDataBySpectrum = GLOBAL_CONTROLLER.line.fragmentNbr;

            this.audioVisualizer.analyser.getByteFrequencyData(this.audioVisualizer.freqs);

            var frequencies = Array.from(this.audioVisualizer.freqs);

            this.UnitSpectrumData = [];

            var i;
            var firstIndex, lastIndex;

            if(GLOBAL_CONTROLLER.behavior.current == 'linear') {
                for(i = 0; i < this.nbrUnitSpectrum; i++) {

                    this.UnitSpectrumData[i] = [];

                    // delimiting the range of the needed values
                    firstIndex = 0;
                    lastIndex = this.nbrDataBySpectrum;

                    // Getting x times the same datas on every Spectrums
                    this.UnitSpectrumData[i] = frequencies.slice(firstIndex, lastIndex);
                }

            } else if(GLOBAL_CONTROLLER.behavior.current == 'anarchist') {

                for(i = 0; i < this.nbrUnitSpectrum; i++) {

                    this.UnitSpectrumData[i] = [];

                    // delimiting the range of the needed values
                    firstIndex = 0;
                    lastIndex = this.nbrDataBySpectrum;

                    // Getting fftsize (1024, 2048 etc...) first values
                    this.UnitSpectrumData[i] = frequencies.slice(firstIndex, GLOBAL_CONTROLLER.line.fftSize);
                    // Random them
                    this.UnitSpectrumData[i].shuffle();
                    // Keeping only the amount of values wanted : nbrDataBySpectrum
                    this.UnitSpectrumData[i] = this.UnitSpectrumData[i].slice(firstIndex, lastIndex);
                }

            } else if (GLOBAL_CONTROLLER.behavior.current == 'mirror') {

                for(i = 0; i < this.nbrUnitSpectrum; i++) {

                    this.UnitSpectrumData[i] = [];

                    // delimiting the range of the needed values
                    firstIndex = 0;
                    lastIndex = this.nbrDataBySpectrum / 2;

                    // Getting x times / 2 the same datas on every Spectrums
                    this.UnitSpectrumData[i] = frequencies.slice(firstIndex, lastIndex);

                    // Getting array as revert
                    var revert = (frequencies.slice(firstIndex, lastIndex)).reverse();

                    // add the reverted part
                    this.UnitSpectrumData[i] = this.UnitSpectrumData[i].concat(revert);
                }
            }

            //clear Canvas
            this.spectrum.canvas.context.clearRect(0, 0, this.spectrum.canvas.width, this.spectrum.canvas.height);

            // Rendering every spectrum
            for(i = 0; i < this.nbrUnitSpectrum; i++) {
                this.spectrum.spectrums[i].render(
                    this.UnitSpectrumData[i],
                    GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.fill.current][i],
                    GLOBAL_THEMES.colors[GLOBAL_CONTROLLER.theme.stroke.current][i]
                );
            }

            this.animationFrame = requestAnimationFrame(this.render.bind(this));
        },

        launch : function () {

            this.launchEls.title = {};
            this.launchEls.title.el = document.querySelector("#title");
            this.launchEls.title.text = this.launchEls.title.el.innerHTML;
            this.launchEls.title.el.innerHTML = this.launchEls.title.el.innerHTML.replace(/(\w)/g, "<span>$&</span>");
            var temp = title.querySelectorAll('span');
            var titleLetters = [];
            var i;
            for (i = 0; i < temp.length; i++) {
                titleLetters[i] = temp[i];
            }
            var titleLetterOrdered = [];
            var position;
            var length = titleLetters.length;
            for (i = 0; i < length; i++) {
                position = Math.floor(titleLetters.length / 2);
                titleLetterOrdered[i] = titleLetters[position];
                titleLetters.splice(position, 1);
            }

            this.launchEls.launch1 = {};
            this.launchEls.launch1.el = document.querySelector("#launch1");

            this.launchEls.launch2 = {};
            this.launchEls.launch2.el = document.querySelector("#launch2");

            this.launchEls.getFile = {};
            this.launchEls.getFile.el = document.querySelector(".getFile");

            var startDelay = 1;

            TweenMax.staggerFrom(titleLetterOrdered, 2, {opacity: 0, delay: startDelay}, .05, "+=1");
            TweenMax.from(this.launchEls.launch1.el, .6, {y: 20, opacity: -5, delay: 2.5 + startDelay});
            TweenMax.from(this.launchEls.launch2.el, 1, {opacity: 0, delay: 3.5 + startDelay});
            TweenMax.from(this.launchEls.getFile.el, 1, {opacity: 0, delay: 4 + startDelay});

            this.contentContainer = document.querySelector('.content-container');

            var tl = new TimelineMax({delay : 5});

            tl.add(this.handleDrop.bind(this));
        },

        songDropped : function(file) {

            if(file.type == "audio/mp3" || file.type == "audio/mpeg") {

                this.songName.value = file.name.split('.mp3');
                this.songName.value.splice(1, 1);
                this.songName.value = this.songName.value.toString();

                var tl, tl2, tl3, tlSongName;

                // Not 1st drag&drop
                if(this.audioVisualizer) {

                    this.audioVisualizer.context.close();
                    cancelAnimationFrame(this.animationFrame);

                    tlSongName = new TimelineMax({ delay : 0.5});
                    tl = new TimelineMax({ delay : 0.5});
                    tl2 = new TimelineMax({ delay : 1.5});
                    tl3 = new TimelineMax({ delay : 2.5});

                    tlSongName.to(this.songName.el, 1, {opacity : 0})
                        .to(this.songName.el, 0, {innerHTML : this.songName.value})
                        .to(this.songName.el, 1, {opacity : 1});

                    tl.add(this.controlPanel.hide.bind(this.controlPanel))
                        .add(this.spectrum.hide.bind(this.spectrum));

                    tl2.add(this.controlPanel.show.bind(this.controlPanel))
                        .add(this.spectrum.show.bind(this.spectrum));

                    tl3.add(this.launchSong.bind(this, file));
                }
                // 1st drag&drop
                else {

                    this.songName.el.textContent = this.songName.value;

                    TweenMax.to(this.launchEls.title.el, 0.5, {opacity : 0});
                    TweenMax.to(this.launchEls.launch1.el, 0.5, {opacity : 0});
                    TweenMax.to(this.launchEls.launch2.el, 0.5, {opacity : 0});
                    TweenMax.to(this.launchEls.getFile.el, 0.5, {opacity : 0});
                    TweenMax.to(this.launchEls.title.el, 0, {innerHTML: 'Sound Spectrum', delay: 0.5});
                    TweenMax.to(this.launchEls.title.el, 1, {top: 10, opacity: 1, fontSize : '4rem', delay: 0.5});
                    TweenMax.to(this.songName.el, 0.5, {opacity : 1, delay: 1.5});

                    TweenMax.to(this.contentContainer, 0, {display: 'flex', delay: 2});

                    tl = new TimelineMax({ delay : 2.5});
                    tl2 = new TimelineMax({ delay : 4.5});

                    tl.add(this.controlPanel.firstShow.bind(this.controlPanel))
                        .add(this.spectrum.firstShow.bind(this.spectrum))
                        .add(this.allowSearchOnTitle.bind(this))
                    .to(document.querySelector('.tip'), 0.5, {opacity : 1});

                    tl2.add(this.launchSong.bind(this, file));
                }
            } else {
                console.log('This is not a .mp3 file');
            }
        },

        launchSong : function(file) {

            var fileReader = new FileReader();

            fileReader.onload = (function(e) {
                var fileResult = e.target.result;

                this.audioVisualizer = new AudioVisualizer(
                    GLOBAL_CONTROLLER.line.smooth,
                    GLOBAL_CONTROLLER.line.fftSize
                );

                this.audioVisualizer.start(fileResult);
            }).bind(this);

            fileReader.readAsArrayBuffer(file);
        },

        searchingForFile : function(e) {

            e.stopPropagation();
            e.preventDefault();

            var file = e.target.files[0];

            this.songDropped(file);

            console.log('searched');
        },

        allowSearchOnTitle : function() {
            this.launchEls.title.el.innerHTML += '<label for="searchLocalFileTitle"><input type="file" accept="audio/*" id="searchLocalFileTitle"></label>';
            TweenMax.to(this.launchEls.title.el.querySelector('label'), 0, {position: 'absolute', width: '100%', height: '100%', left: '0', top: '0', cursor: 'pointer'});
            this.launchEls.title.el.querySelector('#searchLocalFileTitle').addEventListener("change", this.searchingForFile.bind(this));
        }
    };

    return App;

})(spectrum, ControlPanel);

app.init();
