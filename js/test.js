/**
 * Created by Sko on 18/11/2016.
 */

var curve = {

    canvas : {},
    drawValues : {},

    init : function() {

        this.buildCanvas();

        this.buildValues();

        this.draw();
    },

    buildCanvas : function() {

        this.canvas.el = document.querySelector('.spectre');
        this.canvas.context = this.canvas.el.getContext('2d');

        this.canvas.height = this.canvas.el.height;
        this.canvas.width = this.canvas.el.width;

        this.canvas.centerH = this.canvas.height / 2;
        this.canvas.centerW = this.canvas.width / 2;
    },

    buildValues : function() {

        this.drawValues.inflate = 50;

        this.drawValues.path = {};
        this.drawValues.path.fromX = 100;
        this.drawValues.path.fromY = 100;
        this.drawValues.path.toX = 200;
        this.drawValues.path.toY = 200;
        this.drawValues.path.middleX = this.drawValues.path.fromX + (this.drawValues.path.toX - this.drawValues.path.fromX) / 2;
        this.drawValues.path.middleY = this.drawValues.path.fromY - this.drawValues.inflate;

        this.drawValues.bezier1 = {};
        this.drawValues.bezier1.p1X = this.drawValues.path.fromX + (this.drawValues.path.middleX - this.drawValues.path.fromX) * (1/3);
        this.drawValues.bezier1.p1Y = this.drawValues.path.fromY;
        this.drawValues.bezier1.p2X = this.drawValues.path.fromX + (this.drawValues.path.middleX - this.drawValues.path.fromX) * (2/3);
        this.drawValues.bezier1.p2Y = this.drawValues.path.middleY;
        this.drawValues.bezier1.toX = this.drawValues.path.middleX;
        this.drawValues.bezier1.toY = this.drawValues.path.middleY;

        this.drawValues.bezier2 = {};
        this.drawValues.bezier2.p1X = this.drawValues.path.middleX + ((this.drawValues.path.toX - this.drawValues.path.middleX) * (1/3));
        this.drawValues.bezier2.p1Y = this.drawValues.path.middleY;
        this.drawValues.bezier2.p2X = this.drawValues.path.middleX + ((this.drawValues.path.toX - this.drawValues.path.middleX) * (2/3));
        this.drawValues.bezier2.p2Y = this.drawValues.path.toY;
        this.drawValues.bezier2.toX = this.drawValues.path.toX;
        this.drawValues.bezier2.toY = this.drawValues.path.toY;

    },

    draw : function() {

        this.canvas.context.beginPath();
        this.canvas.context.moveTo(this.drawValues.path.fromX, this.drawValues.path.fromY);

        this.canvas.context.bezierCurveTo(
            this.drawValues.bezier1.p1X,
            this.drawValues.bezier1.p1Y,
            this.drawValues.bezier1.p2X,
            this.drawValues.bezier1.p2Y,
            this.drawValues.bezier1.toX,
            this.drawValues.bezier1.toY
        );

        this.canvas.context.bezierCurveTo(
            this.drawValues.bezier2.p1X,
            this.drawValues.bezier2.p1Y,
            this.drawValues.bezier2.p2X,
            this.drawValues.bezier2.p2Y,
            this.drawValues.bezier2.toX,
            this.drawValues.bezier2.toY
        );

        this.canvas.context.stroke();
        this.canvas.context.closePath();
    }
};

curve.init();