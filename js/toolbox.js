/**
 * Created by Ezraman on 24/11/2016.
 */

String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Uint8Array.prototype.shuffle = function(n) {

    function randomInt(mini, maxi) {
        var nb = mini + (maxi+1-mini)*Math.random();
        return Math.floor(nb);
    }

    if(!n)
        n = this.length;
    if(n > 1)
    {
        var i = randomInt(0, n-1);
        var tmp = this[i];
        this[i] = this[n-1];
        this[n-1] = tmp;
        this.shuffle(n-1);
    }
};

Array.prototype.shuffle = function(n) {

    function randomInt(mini, maxi) {
        var nb = mini + (maxi+1-mini)*Math.random();
        return Math.floor(nb);
    }

    if(!n)
        n = this.length;
    if(n > 1)
    {
        var i = randomInt(0, n-1);
        var tmp = this[i];
        this[i] = this[n-1];
        this[n-1] = tmp;
        this.shuffle(n-1);
    }
};
