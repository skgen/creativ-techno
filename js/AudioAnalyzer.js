function AudioVisualizer(smooth, fftSize) {

    this.context = new AudioContext();

    this.el = document.querySelector('body');

    this.setup(smooth, fftSize);
}

AudioVisualizer.prototype.setup = function (smooth, fftSize) {

    this.source = this.context.createBufferSource();

    this.analyser = this.context.createAnalyser();
    this.analyser.fftSize = fftSize;
    this.analyser.smoothingTimeConstant = smooth;

    this.source.connect(this.analyser);

    this.source.connect(this.analyser);
    this.analyser.connect(this.context.destination);

    this.freqs = new Uint8Array(this.analyser.frequencyBinCount);
};

AudioVisualizer.prototype.start = function (buffer) {

    this.context.decodeAudioData(buffer, decodeAudioDataSuccess.bind(this), decodeAudioDataFailed.bind(this));

    function decodeAudioDataSuccess(decodedBuffer) {
        this.source.buffer = decodedBuffer;
        this.source.start();

        this.el.dispatchEvent(new CustomEvent('newSoundLoaded', {
            detail: {
                state : 'loaded'
            },
            bubbles: true
        }));
    }

    function decodeAudioDataFailed() {
        console.log('decodeAudioDataFailed');
    }
};