1. Preparation du dossier

* Se placer dans le dossier du projet

* Créer un dossier : /style

* Dans le dossier style, créer deux dossiers : /sass & /css

* Créer un fichier "master.scss" dans le dossier sass

* Executer depuis le dossier /style : "sass sass/master.scss:css/master.css"

Le chemin a été créé

2. Ecoute du dossier --> step a faire a chaque travail sur le projet

Executer depuis le dossier /style de votre projet : sass --watch sass/master.scss:css/master.css

3. Adaptation du dossier

* Créer un fichier "_nomdufichier.scss" dans le dossier "sass"

* Importez le dans master.scss grâce à : @import "nomdufichier";

Cela permet de split le code pour une meilleure visibilité

